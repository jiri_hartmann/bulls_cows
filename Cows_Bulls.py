"""
Your task is to create a program that would simulate Bulls and Cows game.
First of all, the computer will generate a 4-digit secret number.
The digits must be all different.
Then, in turn, the user tries to guess their computer's number.
The computer prompts the user for a number and after the input has been
received, the computer responds with the number of matching digits.
If the matching digits are in their right positions, they are "bulls",
if in different positions, they are "cows".
"""

import time


def player_name():
    return input("Enter your name: ").replace(" ", "_")


def numb_of_digits():
    while True:
        try:
            number_of_digits = int(input("How many numbers do you want "
                                         "guessing (1-9)? "))
            if number_of_digits in range(1, 10):
                break
            else:
                print("You can input only 1-9 number")
        except ValueError:
            print("You can input only 1-9 number")
    return number_of_digits


def generate_random_number(number_of_digits):
    import random
    rnd_num = random.sample(range(10), number_of_digits)
    rnd_num = [rnd_num[i] for i in range(number_of_digits)]
    return rnd_num


def game_information(number_of_digits):
    print("Hi there!")
    print(f"I've generated a random {number_of_digits} digit number for you.")
    print("Let's play a bulls and cows game.")


def ask_for_number(number_of_digits):
    while True:
        guessing_number = input(f"Enter {number_of_digits} unique numbers: ")
        if guessing_number.isdigit():
            if len(guessing_number) == number_of_digits:
                guessing_number = [int(guessing_number[i]) for i in
                                   range(number_of_digits)]
                if len(guessing_number) == len(set(guessing_number)):
                    break
                else:
                    print(f"Guessing number must have unique numbers.")
            else:
                print(f"Guessing number must have {number_of_digits} "
                      f"numbers.")
        else:
            print(f"Guessing number must contain only digits.")
    return guessing_number


def compute_animal(secret_number, guessing_number):
    cows_bulls = [0, 0]
    for i in range(len(secret_number)):
        if guessing_number[i] in secret_number:
            if guessing_number[i] == secret_number[i]:
                cows_bulls[1] += 1
            else:
                cows_bulls[0] += 1
    return cows_bulls


def tell_result(cow_bulls, counter):
    print(f"{cow_bulls[1]} bulls, {cow_bulls[0]} cows - guess no. "
          f"{counter}\n")


def read_write_results_from_file(file_name, last_result):
    file = open(file_name, "a+")
    file.writelines(last_result)
    file.seek(0)
    results = []
    result = True
    while result:
        result = file.readline().split()
        if result and int(result[1]) == int(last_result[1]):
            results.append(result)
    file.close()
    return results


def tell_end_result(results):
    def result_list():
        stars = "_" * 35
        print("\n        RESULT  LIST")
        print(stars)
        print("|     Name     |  Guess  |  Time  |")
        print(stars)
        results.sort(key=lambda x: (int(x[2]), float(x[3])))
        for result in results:
            print("| {:^12} | {:^7} | {:>6} |".format(result[0], result[2],
                                                      result[3]))
        print(stars)

    print("Congratulations.")
    print(f"{results[-1][0]}, you successfully guessed {results[-1][1]} "
          f"secret numbers and it took you {results[-1][3]} seconds and you "
          f"did it on {results[-1][2]} try.")
    result_list()


def main():
    pl_name = player_name()
    number_of_digits = numb_of_digits()
    game_information(number_of_digits)
    secret_number = generate_random_number(number_of_digits)
    counter = 0
    cows_bulls = [0, 0]
    start_time = time.time()

    while not cows_bulls[1] == number_of_digits:
        guessing_number = ask_for_number(number_of_digits)
        cows_bulls = compute_animal(secret_number, guessing_number)
        counter += 1
        tell_result(cows_bulls, counter)

    time_duration = round(time.time() - start_time, 2)
    last_result = [pl_name + " ", str(number_of_digits) + " ", str(counter)
                   + " ", str(time_duration) + " \n"]
    results = read_write_results_from_file("Cows_Bulls_results.txt",
                                           last_result)
    tell_end_result(results)


main()

